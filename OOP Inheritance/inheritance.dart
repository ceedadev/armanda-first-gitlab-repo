import 'godzilla.dart';
import 'ultramen.dart';

void main(List<String> args) {
  Ultramen u = Ultramen();
  Godzilla g = Godzilla();

  u.levelPoint = 8;
  g.levelPoint = 10;

  print('level point ultramen: ${u.levelPoint} ');
  print('level point godzilla: ${g.levelPoint}');

  print('sifat dari ultramen: ' + u.defense());
  print('sifat dari godzilla: ' + g.attack());
}
